package it.giovanniberti.nine.ui.tilegrid

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.PorterDuff
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.LinearInterpolator
import android.widget.ImageView
import androidx.core.animation.addListener
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.h6ah4i.android.widget.advrecyclerview.draggable.DraggableItemAdapter
import com.h6ah4i.android.widget.advrecyclerview.draggable.ItemDraggableRange
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractDraggableItemViewHolder
import it.giovanniberti.nine.R
import timber.log.Timber
import java.util.*


data class Tile(val bitmap: Bitmap, val correctPosition: Int)

class TileGridViewModelFactory(private val tiles: Int) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return TileGridViewModel(tiles) as T
    }
}

class TileGridViewModel(tiles: Int) : ViewModel() {
    val tilePermutation: MutableLiveData<List<Int>> by lazy {
        MutableLiveData<List<Int>>().also {
            it.value = (0 until tiles).shuffled()
        }
    }
}

class TileGridAdapter(private val context: Context,
                      private val tileGridViewModel: TileGridViewModel):
    ListAdapter<Tile, TileGridAdapter.ViewHolder>(DIFF_CALLBACK),
    DraggableItemAdapter<TileGridAdapter.ViewHolder> {
    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Tile>() {
            override fun areItemsTheSame(oldItem: Tile, newItem: Tile): Boolean {
                return oldItem.bitmap == newItem.bitmap
            }

            override fun areContentsTheSame(oldItem: Tile, newItem: Tile): Boolean {
                return oldItem.bitmap.sameAs(newItem.bitmap)
            }
        }
    }

    init {
        setHasStableIds(true)
    }

    override fun getItemId(position: Int): Long {
        return getItem(position).bitmap.generationId.toLong()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(context)
        return ViewHolder(inflater.inflate(R.layout.tile, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class ViewHolder(view: View) : AbstractDraggableItemViewHolder(view) {
        val tileImageView = view.findViewById<ImageView>(R.id.tile)
        lateinit var tile: Tile

        fun bind(tile: Tile) {
            this.tile = tile
            tileImageView.setImageBitmap(tile.bitmap)

            Timber.d("active = ${dragState.isActive} drag = ${dragState.isDragging} upd = ${dragState.isUpdated}")

            if (dragState.isUpdated) {
                if (dragState.isActive) {
                    tileImageView.alpha = 0.5f
                } else {
                    tileImageView.alpha = 1f
                }
            }
        }
    }

    override fun onCheckCanStartDrag(holder: ViewHolder, position: Int, x: Int, y: Int): Boolean {
        val canDrag = position != holder.tile.correctPosition

        if (!canDrag) {
            val animator = ValueAnimator.ofArgb(Color.parseColor("#ff000000"), Color.parseColor("#88d3102e"))
            animator.duration = 500
            animator.interpolator = LinearInterpolator()
            animator.repeatCount = 1

            animator.addUpdateListener { animation ->
                holder.tileImageView.setColorFilter(animation.animatedValue as Int, PorterDuff.Mode.DARKEN)
            }

            animator.addListener(onEnd = {
                holder.tileImageView.clearColorFilter()
            })

            animator.start()
        }

        return canDrag
    }

    override fun onGetItemDraggableRange(holder: ViewHolder, position: Int): ItemDraggableRange? {
        return null
    }

    override fun onMoveItem(fromPosition: Int, toPosition: Int) {
        val elements = this.currentList.toMutableList()
        Collections.swap(elements, fromPosition, toPosition)

        tileGridViewModel.tilePermutation.value = elements.map { t -> t.correctPosition }
    }

    override fun onCheckCanDrop(draggingPosition: Int, dropPosition: Int): Boolean {
        return getItem(dropPosition).correctPosition != dropPosition
    }

    override fun onItemDragStarted(position: Int) {
        notifyDataSetChanged()
    }

    override fun onItemDragFinished(fromPosition: Int, toPosition: Int, result: Boolean) {
        notifyDataSetChanged()
    }
}

