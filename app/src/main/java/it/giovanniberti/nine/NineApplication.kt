package it.giovanniberti.nine

import android.app.Application
import timber.log.Timber

class NineApplication : Application() {
    override fun onCreate() {
        super.onCreate();

        Timber.plant(Timber.DebugTree());
    }
}