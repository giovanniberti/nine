package it.giovanniberti.nine.utils

import kotlin.collections.ArrayList

fun <T> permute(list: List<T>, permutation: List<Int>): MutableList<T> {
    val listCopy = ArrayList<T>()

    for (i in list.indices) {
        listCopy.add(list[permutation[i]])
    }

    return listCopy
}