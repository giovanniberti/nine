package it.giovanniberti.nine.utils.coil

import android.graphics.*
import androidx.core.graphics.applyCanvas
import coil.bitmap.BitmapPool
import coil.size.Size
import coil.transform.Transformation

class CropTransformation(
    private val offsetX: Double = 0.0,
    private val offsetY: Double = 0.0,
    private val scaleX: Double = 1.0,
    private val scaleY: Double = 1.0
) : Transformation {

    init {
        assert(offsetX in 0.0..1.0)
        assert(offsetY in 0.0..1.0)
        assert(scaleX in 0.0..1.0)
        assert(scaleY in 0.0..1.0)
    }

    override fun key(): String = javaClass.name

    override suspend fun transform(pool: BitmapPool, input: Bitmap, size: Size): Bitmap {
        val paint = Paint(Paint.ANTI_ALIAS_FLAG or Paint.FILTER_BITMAP_FLAG)

        val output = pool.get(
            (scaleX * input.width).toInt(),
            (scaleY * input.height).toInt(),
            Bitmap.Config.ARGB_8888
        )

        output.applyCanvas {
            drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR)

            val matrix = Matrix()
            matrix.setTranslate(
                (-offsetX * input.width).toFloat(),
                (-offsetY * input.height).toFloat()
            )
            val shader = BitmapShader(input, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP)
            shader.setLocalMatrix(matrix)
            paint.shader = shader

            val rect = RectF(0f, 0f,
                (scaleX * input.width).toFloat(),
                (scaleY * input.height).toFloat()
            )
            val path = Path().apply {
                addRect(rect, Path.Direction.CW)
            }

            drawPath(path, paint)
        }

        return output
    }
}