package it.giovanniberti.nine

import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import coil.imageLoader
import coil.request.ImageRequest
import coil.size.OriginalSize
import com.google.android.material.snackbar.Snackbar
import com.h6ah4i.android.widget.advrecyclerview.animator.DraggableItemAnimator
import com.h6ah4i.android.widget.advrecyclerview.draggable.RecyclerViewDragDropManager
import com.h6ah4i.android.widget.advrecyclerview.utils.WrapperAdapterUtils
import it.giovanniberti.nine.ui.tilegrid.Tile
import it.giovanniberti.nine.ui.tilegrid.TileGridAdapter
import it.giovanniberti.nine.ui.tilegrid.TileGridViewModel
import it.giovanniberti.nine.ui.tilegrid.TileGridViewModelFactory
import it.giovanniberti.nine.utils.coil.CropTransformation
import it.giovanniberti.nine.utils.permute
import kotlinx.coroutines.*
import timber.log.Timber


class MainActivity : AppCompatActivity() {
    companion object {
        const val GRID_SPAN: Int = 3
    }

    private val scope = CoroutineScope(Dispatchers.IO)
    private val dragDropManager = RecyclerViewDragDropManager()
    private lateinit var adapter: TileGridAdapter
    private lateinit var tileGrid: RecyclerView

    private val tileGridViewModel: TileGridViewModel by viewModels {
        TileGridViewModelFactory(GRID_SPAN * GRID_SPAN)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        adapter = TileGridAdapter(this, tileGridViewModel)
        val animator = DraggableItemAnimator()
        tileGrid = findViewById(R.id.tile_grid)

        dragDropManager.isCheckCanDropEnabled = true
        dragDropManager.itemMoveMode = RecyclerViewDragDropManager.ITEM_MOVE_MODE_SWAP

        tileGrid.layoutManager = GridLayoutManager(this, GRID_SPAN)
        tileGrid.adapter = dragDropManager.createWrappedAdapter(adapter)
        tileGrid.itemAnimator = animator

        dragDropManager.setInitiateOnLongPress(true)
        dragDropManager.setInitiateOnMove(false)
        dragDropManager.attachRecyclerView(tileGrid)

        val imageLoader = this.imageLoader
        val context = this

        scope.launch {
            val request = ImageRequest.Builder(applicationContext)
                .data("https://picsum.photos/1024")
                .error(R.drawable.local_fallback)
                .allowHardware(false)
                .build()

            val imageBitmap = (imageLoader.execute(request).drawable as BitmapDrawable).bitmap

            val bitmaps = (0 until GRID_SPAN * GRID_SPAN).map {
                async(this.coroutineContext) {
                    val i = it % GRID_SPAN
                    val j = (it - (it % GRID_SPAN)) / GRID_SPAN

                    return@async CropTransformation(
                        offsetX = i.toDouble() / GRID_SPAN,
                        offsetY = j.toDouble() / GRID_SPAN,
                        scaleX = 1.0 / GRID_SPAN,
                        scaleY = 1.0 / GRID_SPAN
                    ).transform(imageLoader.bitmapPool, imageBitmap, OriginalSize)
                }
            }.awaitAll()

            withContext(Dispatchers.Main) {
                tileGridViewModel.tilePermutation.observe(context, { permutation ->
                    Timber.d("new permutation: %s", permutation)

                    if (permutation == (0 until GRID_SPAN * GRID_SPAN).toList()) {
                        showSuccessSnackbar()
                    }

                    val tiles = bitmaps.mapIndexed { pos, bitmap ->
                        Tile(bitmap, pos)
                    }

                    val result = permute(tiles, permutation)
                    adapter.submitList(result)
                })

                val spinner = findViewById<ProgressBar>(R.id.spinner)
                spinner.visibility = View.GONE
            }
        }
    }

    override fun onPause() {
        dragDropManager.cancelDrag()
        super.onPause()
    }

    override fun onDestroy() {
        dragDropManager.release()
        WrapperAdapterUtils.releaseAll(tileGrid.adapter)
        super.onDestroy()
    }

    private fun showSuccessSnackbar() {
        Snackbar.make(findViewById(R.id.tile_grid), R.string.success, Snackbar.LENGTH_INDEFINITE)
            .setAction(R.string.play_again) {
                tileGridViewModel.tilePermutation.value = (0 until GRID_SPAN * GRID_SPAN).shuffled()
            }
            .show()
    }
}
